#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <libgen.h>
#include <dirent.h>
#include <memory.h>
#include <stdbool.h>
#include <errno.h>

typedef struct node_t {
    char name[NAME_MAX];
    char path[PATH_MAX];
    mode_t st_mode;
    struct node_t *next;
} node_t;

node_t *head;
node_t *tail;
int listSize = 0;

char *tarFileName;

mode_t getSt_mode(const char *path) {
    struct stat path_stat;
    if (stat(path, &path_stat) == -1) {
        return NULL;
    }
    return path_stat.st_mode;
}

int compare(const void *a, const void *b) {
    const node_t **node = (const node_t **) a;
    const node_t **node2 = (const node_t **) b;
    return strcmp((*node)->path, (*node2)->path);
}

void sortList() {
    node_t *array[listSize];
    node_t *node = head;
    int i = 0;
    while (node != NULL) {
        array[i] = node;
        i++;
        node = node->next;
    }
    qsort(array, (size_t) listSize, sizeof(node_t *), compare);
    head = array[0];
    tail = array[listSize - 1];
    tail->next = NULL;
    for (i = 0; i < listSize - 1; i++) {
        array[i]->next = array[i + 1];
    }
}

int getChmod(int st_mode) {
    return (st_mode & S_IRUSR) | (st_mode & S_IWUSR) | (st_mode & S_IXUSR) |/*owner*/
           (st_mode & S_IRGRP) | (st_mode & S_IWGRP) | (st_mode & S_IXGRP) |/*group*/
           (st_mode & S_IROTH) | (st_mode & S_IWOTH) | (st_mode & S_IXOTH);/*other*/
}

void addToList(char *name, char *path, mode_t st_mode) {
    node_t *node = malloc(sizeof(node_t));
    strcpy(node->name, name);
    strcpy(node->path, path);
    node->st_mode = st_mode;
    if (head == NULL) {
        head = node;
        tail = node;
    } else {
        tail->next = node;
        tail = node;
    }
    listSize++;
}

void freeList() {
    node_t *node = head;
    while (node != NULL) {
        free(node);
        node = node->next;
    }
}

int countFilesInDir(char *absolutePath) {
    DIR *dir = opendir(absolutePath);
    if (!dir) {
        return -1;
    }
    int count = 0;
    struct dirent *dirent;
    while ((dirent = readdir(dir)) != NULL) {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0)
            count++;
    }
    closedir(dir);
    return count;
}

int saveFileNames(char **array, char *absolutePath) {
    DIR *dir = opendir(absolutePath);
    int i = 0;
    struct dirent *dirent;
    if (!dir) {
        return -1;
    }
    while ((dirent = readdir(dir)) != NULL) {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0) {
            char name[1024];
            strcpy(name, absolutePath);
            strcat(name, "/");
            strcat(name, dirent->d_name);
            array[i] = malloc(strlen(name) * sizeof(char));
            strcpy(array[i], name);
            i++;
        }
    }
    closedir(dir);
    return 0;
}

void checkArguments(int argc, char *argv[]) {
    for (int i = 0; i < argc; i++) {
        mode_t st_mode = getSt_mode(argv[i]);
        if (S_ISDIR(st_mode)) {
            int fileCount = countFilesInDir(argv[i]);
            char *argv2[fileCount];
            saveFileNames(argv2, argv[i]);
            checkArguments(fileCount, argv2);
            for (int j = 1; j < fileCount; j++) {
                free(argv2[j]);
            }
            char *Basename = strdup(basename(argv[i]));
            addToList(Basename, dirname(argv[i]), st_mode);
        } else if (S_ISREG(st_mode)) {
            char *Basename = strdup(basename(argv[i]));
            addToList(Basename, dirname(argv[i]), st_mode);
        } else if (S_ISFIFO(st_mode)) {
            char *Basename = strdup(basename(argv[i]));
            addToList(Basename, dirname(argv[i]), st_mode);
        } else {
            printf("Argument %s nie jest plikiem, katalogiem ani laczem nazwanym. Pomijam.\n", argv[i]);
        }
    }
}

int saveToTar() {
    sortList();
    char magicBytes[] = {0x01, 0x23, 0x45, 0x67};

    FILE *f = fopen(tarFileName, "wb");
    if (!f) {
        return -1;
    }
    fwrite(magicBytes, sizeof(char), 4, f);
    fwrite(&listSize, sizeof(int), 1, f);
    for (node_t *node = head; node; node = node->next) {
        printf("Dodaje %s\n", node->name);
        fwrite(node, sizeof(node_t), 1, f);
        char name[PATH_MAX];
        strcpy(name, node->path);
        strcat(name, "/");
        strcat(name, node->name);
        char *array = malloc(strlen(name) * sizeof(char));
        strcpy(array, name);
        size_t fileSize;
        char *byteArray;
        if (S_ISDIR(node->st_mode) || S_ISFIFO(node->st_mode)) {
            fileSize = 1;
            byteArray = malloc(fileSize + 1);
        } else {
            FILE *file = fopen(array, "rb");
            fseek(file, 0, SEEK_END);
            fileSize = (size_t) ftell(file);
            fseek(file, 0, SEEK_SET);

            byteArray = malloc(fileSize + 1);
            fread(byteArray, fileSize, 1, file);
            fclose(file);
        }
        byteArray[fileSize] = 0;
        fwrite(&fileSize, sizeof(long), 1, f);
        fwrite(byteArray, fileSize * sizeof(char), 1, f);
        free(byteArray);
    }
    fclose(f);
    return 0;
}

int extractFile(node_t *node, char byteArray[], long size) {
    printf("Rozpakowuje %s\n", node->name);
    char temp[PATH_MAX];
    strcpy(temp, "extracted");
    struct stat st = {0};
    if (stat(temp, &st) == -1) {
        mkdir(temp, 0700);
    }
    strcat(temp, "/");
    strcat(temp, node->path);
    strcat(temp, "/");
    strcat(temp, node->name);
    if (S_ISDIR(node->st_mode)) {
        mkdir(temp, node->st_mode);
    } else if (S_ISFIFO(node->st_mode)) {
        mkfifo(temp, node->st_mode);
    } else {
        FILE *file = fopen(temp, "wb");
        if (!file) {
            return -1;
        }
        fwrite(byteArray, size * sizeof(char), 1, file);
        if (chmod(temp, node->st_mode) < 0) {
            fprintf(stderr, "%s: error in chmod(%s, %s) - %d (%s)\n",
                    "mytar", temp, "", errno, strerror(errno));
        }
        fclose(file);
    }
}

int readFromTar(bool extract) {
    char magicBytes[4];
    char magicBytes2[] = {0x01, 0x23, 0x45, 0x67};
    FILE *f = fopen(tarFileName, "rb");
    if (!f) {
        return -1;
    }
    fread(magicBytes, sizeof(char), 4, f);
    for (int i = 0; i < 4; i++) {
        if (magicBytes[i] != magicBytes2[i]) {
            return -2;
        }
    }
    int listSize2;
    fread(&listSize2, sizeof(int), 1, f);
    for (int i = 0; i < listSize2; i++) {
        node_t node;
        fread(&node, sizeof(node_t), 1, f);
        long fileSize;
        fread(&fileSize, sizeof(long), 1, f);
        char byteArray[fileSize];
        fread(byteArray, fileSize * sizeof(char), 1, f);
        if (extract) {
            extractFile(&node, byteArray, fileSize);
        }
        addToList(node.name, node.path, node.st_mode);
    }
    fclose(f);
    return 0;
}

void printList() {
    for (node_t *node = head; node; node = node->next) {
        printf("\nname: %s\npath: %s\npermissions: %o\nis symbolic: %d\nis directory: %d\nis FIFO: %d\n",
               node->name, node->path,
               getChmod(node->st_mode), S_ISLNK(node->st_mode), S_ISDIR(node->st_mode), S_ISFIFO(node->st_mode));
    }
}

int main(int argc, char *argv[]) {
    if (argc >= 2) {
        if (strcmp(argv[1], "--create") == 0 || strcmp(argv[1], "-c") == 0) {
            if (argc >= 4) {
                tarFileName = argv[2];
                checkArguments(argc - 3, argv + 3);
                saveToTar();
            }
        } else if (strcmp(argv[1], "--list") == 0 || strcmp(argv[1], "-l") == 0) {
            if (argc >= 3) {
                tarFileName = argv[2];
                int result = readFromTar(false);
                printf("read result: %d\n", result);
            }
        } else if (strcmp(argv[1], "--extract") == 0 || strcmp(argv[1], "-x") == 0) {
            if (argc >= 3) {
                tarFileName = argv[2];
                int result = readFromTar(true);
                printf("read result: %d\n", result);
            }
        } else if (strcmp(argv[1], "--test") == 0 || strcmp(argv[1], "-t") == 0) {
            if (argc >= 3) {
                tarFileName = argv[2];
                if (readFromTar(false) == 0) {
                    printf("Plik jest poprawny.");
                } else {
                    printf("Plik jest niepoprawny.");
                }
            }
        } else {
            printf("Niepoprawny argument!");
        }
    } else {
        printf("Nie podano arguentow!");
    }
    printList();
    freeList();
    return 0;
}